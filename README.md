* payment pull script - script that fetches payment pull data.
* payments.docx - has some info about payment pull data.
* queries_used.docx - has queries that are used to pull data from the database.
* Payment_pull_data_21_may_2021 - sample payment pull data file that is shared with the respective teams.
* payment_pull_results_21_may_2021 - sample result file for a cycle.
